﻿namespace TateApp.Common.Models
{
    using System.Collections.Generic;


    #region Login Request
    public class LoginRequest
    {
        public LoginUsuario Usuario { get; set; }
        public LoginDispositivo Dispositivo { get; set; }
    }

    public partial class LoginUsuario
    {
        public string Usuario { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
    }

    public partial class LoginDispositivo
    {
        public string Token { get; set; } = string.Empty;
        public int Sistema { get; set; } = 0;
    }

    #endregion

    #region Login Response
    public class LoginResponse
    {
        public ResponseResultado Respuesta { get; set; }
        public ResponseUsuario Usuario { get; set; }
        public ResponseSistema Sistema { get; set; }
        public List<ResponseMenu> Menu { get; set; }
    }

    public partial class ResponseResultado
    {
        public string Resultado { get; set; } = string.Empty;

        public string Mensaje { get; set; } = string.Empty;
    }

    public partial class ResponseUsuario
    {
        public long UsuarioId { get; set; } = 0;

        public string Nombre { get; set; } = string.Empty;

        public string TokenApiWeb { get; set; } = string.Empty;
    }
    public partial class ResponseSistema
    {
        public int VersionMinima { get; set; } = 0;

        public int VersionActual { get; set; } = 0;

        public string VersionActualNombre { get; set; } = string.Empty;

        public string URL { get; set; } = string.Empty;

        public string URLRegistro { get; set; } = string.Empty;
    }
    public partial class ResponseMenu
    {
        public string Grupo { get; set; } = string.Empty;

        public string GrupoTitulo { get; set; } = string.Empty;

        public string Tipo { get; set; } = string.Empty;

        public string Icono { get; set; } = string.Empty;
        public string Titulo { get; set; } = string.Empty;
        public string Nombre { get; set; } = string.Empty;

        public string Destino { get; set; } = string.Empty;

        public string Sistema { get; set; } = string.Empty;
        public int Orden { get; set; } = 0;
        public string Credenciales { get; set; } = "{}";



    }


    

    #endregion














}