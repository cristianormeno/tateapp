﻿namespace TateApp.Common.Models
{
    public partial class ApiResponseModel
    {
        public bool IsSuccess { get; set; } = false;

        public string Message { get; set; } = string.Empty;

        public object Result { get; set; }
    }
}
