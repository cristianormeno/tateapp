﻿using Foundation;
using TateApp.Interfaces;
using TateApp.iOS.Implementations;
using Xamarin.Forms;

[assembly: Dependency(typeof(IBaseUrl_iOS))]
namespace TateApp.iOS.Implementations
{
    public class IBaseUrl_iOS : IBaseUrl
    {
        public string Get()
        {
            return NSBundle.MainBundle.BundlePath;
        }
    }
}