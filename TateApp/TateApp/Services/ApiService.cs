﻿namespace TateApp.Services
{
    using System;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Plugin.Connectivity;
    using Models;
    using TateApp.Common.Models;

    class ApiService 
    {
        public async Task<ApiResponseModel> CheckConnection() 
        {
            if (!CrossConnectivity.Current.IsConnected) 
            {
                return new ApiResponseModel 
                {
                    IsSuccess = false,
                    Message = "Error de conexión 1"//Languages.ConnectionError1,
                };
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable) 
            {
                return new ApiResponseModel 
                {
                    IsSuccess = false,
                    Message = "Error de Conexión 2"//Languages.ConnectionError2,
                };
            }

            return new ApiResponseModel 
            {
                IsSuccess = true,
            };
        }

        public async Task<LoginResponse> Login<T>( string urlBase, string servicePrefix, string controller, T model) 
        {
            try 
            {
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request,Encoding.UTF8,"application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode) 
                {
                    return new LoginResponse
                    {
                        Respuesta = new ResponseResultado()
                        {
                            Resultado = "ERROR",
                            Mensaje = response.StatusCode.ToString()
                        },
                        Usuario = new ResponseUsuario()
                        {
                            UsuarioId = 0,
                            Nombre = string.Empty,
                            TokenApiWeb = string.Empty
                        }
                    };
                }

                var result = await response.Content.ReadAsStringAsync();
                LoginResponse Resultado = JsonConvert.DeserializeObject<LoginResponse>(result);

                return Resultado;
            }
            catch (Exception ex) 
            {
                return new LoginResponse 
                {
                    Respuesta = new ResponseResultado() 
                    {
                        Resultado = "ERROR",
                        Mensaje = ex.Message,
                    },
                    Usuario = new ResponseUsuario() 
                    {
                        UsuarioId = 0,
                        Nombre = ""
                    }
                };
                
            }
        }

        public async Task<bool> ValidarToken( string urlBase, string servicePrefix, string controller, string tokenType, string accessToken)
        {
            try
            {
                var content = new StringContent( "", Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                    return false;
                else
                    return true;


            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<ScannerPageResponse> ScannerApi<T>(string urlBase, string servicePrefix, string controller, T model)
        {
            try
            {
                var request = JsonConvert.SerializeObject(model);
                var content = new StringContent(request, Encoding.UTF8, "application/json");
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new ScannerPageResponse
                    {
                        URL = string.Empty
                    };
                }

                
                var result = await response.Content.ReadAsStringAsync();
                ScannerPageResponse Resultado = new ScannerPageResponse();
                Resultado= JsonConvert.DeserializeObject<ScannerPageResponse>(result);

                return Resultado;
            }
            catch (Exception ex)
            {
                return new ScannerPageResponse
                {
                    URL = string.Empty                    
                };

            }
        }

    }
}
