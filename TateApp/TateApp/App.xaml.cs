﻿namespace TateApp
{
    using Helpers;
    using Models;
    using Notifications;
    using Plugin.Badge;
    using Plugin.FirebasePushNotification;
    using System;
    using ViewModels;
    using Views;
    using Xamarin.Forms;

    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator { get; internal set; }
        public static MasterView Master { get; internal set; }
        #endregion

        #region Constructors
        public App(NotificacionModel notificacion)
        {
            InitializeComponent();
            NotificationsServices.NotificacionOpen();
        }

        public App()
        {
            InitializeComponent();
            try
            {
                //Setear Badge
                SetearBadge();

                //Obtiene el Token del dispositivo para Notificaciones
                CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
                {

                    //Recupero el Token del dispositivo para las notificaciones
                    Settings.TokenDispositivo = p.Token;
                    System.Diagnostics.Debug.WriteLine($"TOKEN: {p.Token}");
                };

                //Cuando ingresa una notificación 
                CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
                {
                    //Notificación recibida
                    System.Diagnostics.Debug.WriteLine("Received");
                    if (p.Data != null)
                    {
                        NotificationsServices.CreateNotificacion(p.Data);
                    }

                };

                //Cuando se abre la aplicación atravez de una notificación
                CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
                {
                    NotificacionModel NotificacionCreada = NotificationsServices.NotificacionCreadaAcceso(p.Data);
                    Settings.NotificacionSeleccionada = NotificacionCreada;
                    if (Settings.NotificacionSeleccionada != null && !string.IsNullOrEmpty(Settings.TokenDispositivo) && !string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
                    {
                        MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(Settings.NotificacionSeleccionada);

                        if (Device.RuntimePlatform.ToString().ToUpper() != "ANDROID")
                        {
                            Settings.NotificacionSeleccionada = null;
                        }
                        if (Device.RuntimePlatform.ToString().ToUpper() != "IOS")
                        {
                            Settings.NotificacionSeleccionada = null;
                        }
                        App.Navigator.PushAsync(new NotificacionView());
                    }
                };

                //Obtener datos del usuario y menú

                // seteo los datos del usuario como anónimo
                UsuarioModel usuario = Config.UsuarioAnonimo();
                //Creo la instancia de la MainViewModel
                MainViewModel mainViewModel = MainViewModel.GetInstance();
               Helpers.Common comunes = new Helpers.Common();

                //Consultar por la opción de recordar usuario
                if (Settings.Recordarme == true)
                {
                    // obtengo los datos del usuario recordado en el dispositivo
                    usuario = Settings.Usuario;
                    usuario.Registrado = true;

                    // valido que el Token Web esté vigente
                    if (string.IsNullOrEmpty(usuario.TokenApiWeb))
                    {
                        // está vacío, seteo el usuario como anónimo
                        usuario = Config.UsuarioAnonimo();
                    }
                    else
                    {
                        if (!comunes.TokenVigente(usuario.TokenApiWeb))
                        {
                            // no está vigente, seteo el usuario como anónimo
                            usuario = Config.UsuarioAnonimo();
                        }
                    }
                }

                Settings.Usuario = usuario;
                //Intento hacer login 
                Autenticar aut = new Autenticar();

                aut.Login();
                Application.Current.MainPage = new MasterView();
            }
            catch (Exception ex)
            { }
        }

        private void SetearBadge()
        {
            Settings.NumeroNotificaciones = NotificationsServices.NoLeidas();
            if (Settings.NumeroNotificaciones == 0)
            {
                CrossBadge.Current.ClearBadge();
            }
        }

        

        

        #endregion



        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
