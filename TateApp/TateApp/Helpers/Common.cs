﻿namespace TateApp.Helpers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Threading.Tasks;
    using TateApp.Services;
    using Xamarin.Forms;

    public class Common
    {
        #region Services

        private ApiService apiService;

        #endregion

        #region Properties

        public int GetSistemaDispositivo
        {
            get
            {
                if (Device.RuntimePlatform.ToString().ToUpper() == "ANDROID")
                    return 1;
                else
                {
                    if (Device.RuntimePlatform.ToString().ToUpper() == "IOS")
                        return 2;
                    else
                        return 0;
                }
            }
        }

        public string GetTokenSistema
        {
            get
            {
                if (string.IsNullOrEmpty(Settings.TokenDispositivo))
                {
                    try
                    {
                        Settings.TokenDispositivo = Device.RuntimePlatform.ToString().Trim().ToLower() + "_" + Guid.NewGuid().ToString().Trim();
                    }
                    catch
                    {
                        Settings.TokenDispositivo = "";
                    }

                }
                return Settings.TokenDispositivo;
            }
        }

        #endregion

        #region Methods

        public async Task<bool> ValidarToken()
        {
            try
            {
                this.apiService = new ApiService();
                // llamo al ws
                string api = "";
                try
                { api = Application.Current.Resources["APIWeb"].ToString(); }
                catch
                { api = "http://200.58.108.18:8086"; }

                var resultado = await this.apiService.ValidarToken(api, "PC/", "ping", "Bearer", Settings.Usuario.TokenApiWeb);

                return resultado;
            }
            catch
            {
                return false;
            }


        }

        public bool TokenVigente(string tokenWeb)
        {
            try
            {

                //Obtengo los datos de expiración del Token
                var jwthandler = new JwtSecurityTokenHandler();
                var jwttoken = jwthandler.ReadToken(tokenWeb as string);
                var expDate = jwttoken.ValidTo;

                // Validar que el token esté vigente
                if (expDate > DateTime.Now)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }


        }

        #endregion
    }
}
