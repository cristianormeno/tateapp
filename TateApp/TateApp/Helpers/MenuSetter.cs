﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TateApp.Common.Models;

namespace TateApp.Helpers
{
    public static class MenuSetter
    {
        public static List<ResponseMenu> Setter(List<ResponseMenu> responseMenus)
        {
            List<ResponseMenu> menu = new List<ResponseMenu>();

            int i = 0;
            int salir = -1;

            salir = responseMenus.FindIndex(item => item.Titulo.ToLower().Trim() == "salir");

            Type type = typeof(Helpers.Languages);
            PropertyInfo[] properties = type.GetProperties();
            
            foreach (ResponseMenu Item in responseMenus)
            {
                Item.Nombre = Item.Titulo;
                int prop= -1;
                prop = Array.IndexOf(properties.Select(x => x.Name).ToArray(),Item.Titulo);
                if (prop >= 0)
                {
                    try 
                    {
                        Item.Titulo = properties[prop].GetValue(null).ToString().Trim();
                    } catch { }
                    
                }

                menu.Add(Item);
                i++;
            }

            if (salir < 0)
            {
                //Agrego el menú Salir
                menu.Add(new ResponseMenu()
                {
                    Tipo = "app",
                    Icono = "AccountCircleOutline",
                    Titulo = Languages.Salir,
                    Nombre="Salir",
                    Destino = ""
                });
            }

            return menu;

        }
    }
}
