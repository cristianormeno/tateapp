﻿using TateApp.Models;

namespace TateApp.Helpers
{
    public static class Config
    {

        public static UsuarioModel UsuarioAnonimo()
        {
            UsuarioModel usu = new UsuarioModel()
            {
                UsuarioID = 0,
                Usuario = "anonimo",
                Contraseña = "123456",
                Nombre = "",
                URL = "",
                TokenApiWeb = "",
                Registrado = false
            };
            return usu;
        }
    }
}
