﻿namespace TateApp.Models
{
    public class UsuarioModel
    {
        public long UsuarioID { get; set; } = 0;
        public string Usuario { get; set; } = string.Empty;
        public string Contraseña { get; set; } = string.Empty;
        public string TokenApiWeb { get; set; } = string.Empty;
        public string Nombre { get; set; } = string.Empty;
        public string URL { get; set; } = string.Empty;
        public bool Registrado { get; set; } = false;
        public string URLRegistro { get; set; } = string.Empty;
    }
}

