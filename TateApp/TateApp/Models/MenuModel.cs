﻿using System.Collections.ObjectModel;
using TateApp.ViewModels;

namespace TateApp.Models
{
    public class MenuModel : ObservableCollection<MenuItemViewModel>
    {
        public string Grupo { get; set; } = string.Empty;
        public string GrupoTitulo { get; set; } = string.Empty;
    }    
}
