﻿namespace TateApp.ViewModels
{
    using Helpers;
    using System.Diagnostics;
    using Xamarin.Forms;

    public class HomeViewModel : BaseViewModel
    {
        #region Properties
        
        #endregion

        #region Attributes
    
        private WebView navegador;

        

        #endregion

        #region Constructors
        public HomeViewModel()
        {
             
        }
        public HomeViewModel(INavigation navService, WebView browser)
        {
            
            navegador = browser;
            try
            {
                Acr.UserDialogs.UserDialogs.Instance.ShowLoading(Languages.EsperaUnMomento, Acr.UserDialogs.MaskType.Gradient);
            }
            catch { }
            //browser.Source = Settings.Usuario.URL;
            var mainViewModel = MainViewModel.GetInstance();
            if (string.IsNullOrEmpty(mainViewModel.URL))
            {
                browser.Source = Settings.Usuario.URL;
            }
            else
            {
                browser.Source = mainViewModel.URL;
            }

            browser.Navigated += WebBrowser_Navigated;

            
            try { Acr.UserDialogs.UserDialogs.Instance.HideLoading(); } catch { }

 
        }




        #endregion

        #region Comandos
        
        #endregion

        #region Metodos
        private void WebBrowser_Navigated(object sender, WebNavigatedEventArgs e)
        {
            Debug.WriteLine($"WebBrowser_Navigated -> {e.Url}");
            try { Acr.UserDialogs.UserDialogs.Instance.HideLoading(); } catch { }
        }

        

        #endregion

                

        
    }
}
