﻿namespace TateApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using TateApp.Common.Models;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class MenuItemViewModel : ResponseMenu
    {
        
        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;
            ScannerPageRequest scannerPageRequest = new ScannerPageRequest();

            switch (this.Tipo)
            {
                case "app":
                    switch (this.Nombre.ToLower())
                    {
                        case "home":
                            MainViewModel.GetInstance().URL = string.Empty;
                            MainViewModel.GetInstance().Home = new HomeViewModel();
                            App.Navigator.PushAsync(new HomeView());
                            break;
                        case "notificaciones":
                            MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                            App.Navigator.PushAsync(new NotificacionesView());
                            break;
                        case "salir":
                            //Settings.Recordarme = false;
                            //UsuarioModel usu = new UsuarioModel()
                            //{
                            //    Usuario = null,
                            //    Contraseña = null,
                            //    TokenApiWeb = null,
                            //    UsuarioID = 0,
                            //    URL = null
                            //};
                            var mainViewModel = MainViewModel.GetInstance();
                            UsuarioModel usu = Config.UsuarioAnonimo();

                            //Si el usuario es anónimo, borro los datos para que no se muestren en el login
                            if (Settings.Usuario.Usuario==usu.Usuario)
                            {
                                mainViewModel.Login.Usuario = "";
                                mainViewModel.Login.Contraseña = "";
                            }

                            //Si el recordarme está en falso, que no se muestrn los datos del login
                            if (!Settings.Recordarme)
                            {
                                mainViewModel.Login.Usuario = "";
                                mainViewModel.Login.Contraseña = "";
                            }


                            //mainViewModel.Usuario = usu;
                            //Settings.Usuario = usu;

                            Application.Current.MainPage = new NavigationPage(
                                new LoginView());
                            break;
                    }
                    break;
                case "scan":
                    scannerPageRequest = new ScannerPageRequest()
                    {
                        UsuarioId = Settings.Usuario.UsuarioID,
                        Sistema = this.Sistema,
                        Destino = this.Destino,
                        Valor = string.Empty,
                        EnviarApi = false
                    };
                    MainViewModel.GetInstance().Scan = new ScanViewModel(scannerPageRequest);
                    App.Navigator.PushAsync(new ScanView() { Title = this.Titulo });
                    break;
                case "web":
                    MainViewModel.GetInstance().URL = this.Destino;
                    MainViewModel.GetInstance().Home = new HomeViewModel();
                    App.Navigator.PushAsync(new HomeView() { Title = this.Titulo });
                    break;
                case "scanapi":
                    scannerPageRequest = new ScannerPageRequest()
                    {
                        UsuarioId = Settings.Usuario.UsuarioID,
                        Sistema = this.Sistema,
                        Destino = this.Destino,
                        Valor = string.Empty,
                        EnviarApi=true
                    };
                    MainViewModel.GetInstance().Scan = new ScanViewModel(scannerPageRequest);
                    App.Navigator.PushAsync(new ScanView() { Title = this.Titulo });
                    break;
            }











        }
        #endregion
    }
}
