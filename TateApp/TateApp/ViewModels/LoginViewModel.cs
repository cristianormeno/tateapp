﻿namespace TateApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using Services;
    using TateApp.Common.Models;
    using System;
    using System.Collections.Generic;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Servicios
        private ApiService apiService;
        #endregion

        #region Attributes
        private string usuario;
        private string mandante;
        private string contraseña;

        private bool estaHabilitado;
        private bool recordarme;
        private bool visibleRegistrarse;
        #endregion

        #region Properties
        public string Usuario
        {
            get { return this.usuario; }
            set { SetValue(ref this.usuario, value); }
        }

        public string Mandante
        {
            get { return this.mandante; }
            set { SetValue(ref this.mandante, value); }
        }

        public string Contraseña
        {
            get { return this.contraseña; }
            set { SetValue(ref this.contraseña, value); }
        }

        public bool Recordarme
        {
            get { return this.recordarme; }
            set { SetValue(ref this.recordarme, value); }
        }

        public bool EstaHabilitado
        {
            get { return this.estaHabilitado; }
            set { SetValue(ref this.estaHabilitado, value); }
        }

        public bool VisibleRegistrarse {
            get { return this.visibleRegistrarse; }
            set { SetValue(ref this.visibleRegistrarse, value); } 
        }
        #endregion

        #region Constructores
        public LoginViewModel()
        {
            this.apiService = new ApiService();

            this.Recordarme = true;
            this.EstaHabilitado = true;
            this.Usuario = "";
            this.Contraseña = "";
            this.VisibleRegistrarse = true;

            
        }
        #endregion

        #region Comandos

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        public ICommand CancelCommand
        {
            get 
            {
                return new RelayCommand(Cancel);
            }


        }

        public ICommand LogoutCommand
        {
            get
            {
                return new RelayCommand(Logout);
            }


        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(Register);
            }


        }

        
        #endregion

        #region Metodos
        private async void Login()
        {
            try
            {
                Acr.UserDialogs.UserDialogs.Instance.ShowLoading(Languages.EsperaUnMomento, Acr.UserDialogs.MaskType.Gradient);
            }
            catch { }

            // Validar que los campos requeridos estén completos
            if (string.IsNullOrEmpty(this.Usuario))
            {
                MostrarMensaje(
                    Languages.Error,
                    Languages.DebesIngresarUnUsuario);
                return;
            }

            if (string.IsNullOrEmpty(this.Contraseña))
            {
                MostrarMensaje(
                   Languages.Error,
                   Languages.DebesIngresarUnaContraseña);
                return;
            }

           

            // Deshabilito la vista
            this.EstaHabilitado = false;

            // Obtengo la prueba de conexión
            var conexion = await this.apiService.CheckConnection();

            if (!conexion.IsSuccess)
            {
                this.EstaHabilitado = true;
                MostrarMensaje(Languages.Error, conexion.Message);
                return;
            }


            // Obtengo el sistema operativo del dispositivo
            Common comunes = new Common();
            int Sistema = comunes.GetSistemaDispositivo;
            string Token = comunes.GetTokenSistema;


            LoginRequest Datos = new LoginRequest
            {
                Usuario = new LoginUsuario()
                {
                    Usuario = this.Usuario,
                    Password = this.Contraseña
                },
                Dispositivo = new LoginDispositivo()
                {
                    Token = Token,
                    Sistema = Sistema
                }
            };

            // llamo al ws
            var api = Application.Current.Resources["APIWeb"].ToString();
            var apiBase = Application.Current.Resources["APIWebBase"].ToString() + "/";

            LoginResponse resultado = new LoginResponse();
            resultado = await this.apiService.Login(api, apiBase, "Login", Datos);



            if (resultado == null)
            {
                MostrarMensaje("Languages.Error", Languages.AlgoSalioMal);
                return;
            }

            if (string.IsNullOrEmpty(resultado.Usuario.TokenApiWeb))
            {
                MostrarMensaje("Languages.Error", resultado.Respuesta.Mensaje);
                return;
            }

            if (!VersionUnica.ValidarVersion(resultado.Sistema.VersionMinima))
            {
                MostrarMensaje("Languages.Error", Languages.VersionMinima);
                return;
            }

            UsuarioModel usu = new UsuarioModel
            {
                UsuarioID = resultado.Usuario.UsuarioId,
                Usuario = this.Usuario,
                Contraseña = this.Contraseña,
                TokenApiWeb = resultado.Usuario.TokenApiWeb,
                Nombre = resultado.Usuario.Nombre,
                URL = resultado.Sistema.URL,
                URLRegistro=resultado.Sistema.URLRegistro
            };

            List<ResponseMenu> Menu = new List<ResponseMenu>();
            Menu = MenuSetter.Setter(resultado.Menu);
            Settings.Menu = Menu;

            if (usu.Usuario != "anonimo" && usu.UsuarioID > 0)
            {
                usu.Registrado = true;
            }
            else
            {
                usu.Registrado = false;
            }

            // Guardo los datos del usuario
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Usuario = usu;
            Settings.Usuario = usu;
            mainViewModel.Menu = Menu;
            mainViewModel.LoadMenu();


            if (this.Recordarme)
            {
                Settings.Recordarme = true;
            }
            else
            {
                Settings.Recordarme = false;
            }

            //Llamo a la siguiente vista
            mainViewModel.Home = new HomeViewModel();
            mainViewModel.URL = Settings.Usuario.URL;
            Application.Current.MainPage = new MasterView();


            try { Acr.UserDialogs.UserDialogs.Instance.HideLoading(); } catch { }

        }

        private void Cancel()
        {
            var mainViewModel = MainViewModel.GetInstance();
            //Llamo a la siguiente vista
            //mainViewModel.Home = new HomeViewModel();
            if(mainViewModel.Usuario.Usuario=="")
            {
                mainViewModel.Usuario = Settings.Usuario;
            }
            mainViewModel.URL = Settings.Usuario.URL;
            mainViewModel.Home = new HomeViewModel();
            Application.Current.MainPage = new MasterView();
        }

        private void Logout()
        {
            if (Settings.Usuario.Usuario != "anonimo")
            {
                Settings.Recordarme = false;
                Settings.Usuario = Config.UsuarioAnonimo();
                Settings.Menu.Clear();


                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Menu.Clear();
                mainViewModel.Usuario = Config.UsuarioAnonimo();
                this.Usuario = Settings.Usuario.Usuario;
                this.Contraseña = Settings.Usuario.Contraseña;
                mainViewModel.URL = Settings.Usuario.URL;
                Login();
            }
            else {
                Cancel();
            }
                       
        }

        private void Register()
        {
            var mainViewModel = MainViewModel.GetInstance();
            if (!string.IsNullOrEmpty(Settings.Usuario.URLRegistro)) 
            {
                mainViewModel.URL = Settings.Usuario.URLRegistro;
            }

            
            mainViewModel.Home = new HomeViewModel();
            Application.Current.MainPage = new MasterView();
        }


        private void MostrarMensaje(string Titulo, string Mensaje)
        {
            //UserDialogs.Instance.Alert(Mensaje,"Error","Aceptar");
            Application.Current.MainPage.DisplayAlert(Mensaje, "Error", Languages.Aceptar);
            try { Acr.UserDialogs.UserDialogs.Instance.HideLoading(); } catch { }
            this.EstaHabilitado = true;
        }


        #endregion
    }
}
