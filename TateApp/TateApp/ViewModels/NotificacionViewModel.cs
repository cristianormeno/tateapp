﻿namespace TateApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using TateApp.Helpers;
    using TateApp.Views;
    using Notifications;
    using System.Windows.Input;

    public class NotificacionViewModel : BaseViewModel
    {
        #region Attributes
        #endregion

        #region Propperties
        public NotificacionModel Notificacion {get; set;}
        #endregion

        #region Constructors
        public NotificacionViewModel(NotificacionModel notificacion)
        {
            this.Notificacion = notificacion;
            MarcarLeidaNotificacion();
        }

        #endregion

        #region Commands
        public ICommand EliminarCommand
        {
            get { return new RelayCommand(EliminarNotificacion); }
        }
        #endregion

        #region Methods

        private void MarcarLeidaNotificacion()
        {
            NotificationsServices.MarcarLeidaNotificacion(this.Notificacion.Id);
            NotificationsServices.NotificacionesPendientes();
        }

        private void EliminarNotificacion()
        {
            Acr.UserDialogs.UserDialogs.Instance.Confirm(new Acr.UserDialogs.ConfirmConfig
            {
                CancelText = Languages.No,
                OkText = Languages.Si,
                Message = Languages.EliminarNotificacionMensaje,
                OnAction = async (r) =>
                {
                    if (r)
                    {
                        NotificationsServices.DeleteNotificacion(this.Notificacion.Id);
                        NotificationsServices.NotificacionesPendientes();
                        MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                        await App.Navigator.PushAsync(new NotificacionesView());

                    }
                }
            });


        }


        #endregion
    }
}
