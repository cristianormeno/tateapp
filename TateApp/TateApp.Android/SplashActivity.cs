﻿namespace TateApp.Droid
{

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Plugin.FirebasePushNotification;
    using Android.Util;

    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        static readonly string TAG = "X:" + typeof(SplashActivity).Name;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Log.Debug(TAG, "SplashActivity.OnCreate");


            System.Threading.Thread.Sleep(2000);

            this.StartActivity(typeof(MainActivity));

            FirebasePushNotificationManager.ProcessIntent(this, Intent);
        }

        protected override void OnNewIntent(Intent intent)
        {
            FirebasePushNotificationManager.ProcessIntent(this, intent);
            base.OnNewIntent(intent);
        }
    }
}