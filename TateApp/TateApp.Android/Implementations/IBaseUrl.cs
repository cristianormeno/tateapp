﻿using TateApp.Droid.Implementations;
using TateApp.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(IBaseUrlAndroid))]

namespace TateApp.Droid.Implementations
{
    public class IBaseUrlAndroid : IBaseUrl
    {
        public string Get()
        {
            return "file:///android_asset/";
        }
    }
}