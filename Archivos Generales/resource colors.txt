<Color x:Key="ColorPrimary">#2196F3</Color>
            <Color x:Key="ColorPrimaryDark">#1976D2</Color>
            <Color x:Key="ColorPrimaryLight">#BBDEFB</Color>

            <Color x:Key="ColorOnPrimary">#FFFFFF</Color>
            <Color x:Key="ColorOnPrimaryDark">#FFFFFF</Color>
            <Color x:Key="ColorOnPrimaryLight">#212121</Color>


            <Color x:Key="ColorSecondary">#03A9F4</Color>
            <Color x:Key="ColorSecondaryDark">#0288d1</Color>
            <Color x:Key="ColorSecondaryLigth">#f5f5f5</Color>

            <Color x:Key="ColorOnSecondary">#FFFFFF</Color>
            <Color x:Key="ColorOnSecondaryDark">#FFFFFF</Color>
            <Color x:Key="ColorOnSecondaryLigth">#212121</Color>

            <Color x:Key="ColorBackground">#FFFFFF</Color>
            <Color x:Key="ColorSurface">#fafafa</Color>
            <Color x:Key="ColorError">#ff5722</Color>

            <Color x:Key="ColorOnBackround">#212121</Color>
            <Color x:Key="ColorOnSurface">#757575</Color>
            <Color x:Key="ColorOnError">#FFFFFF</Color>
            <Color x:Key="ColorTextoLight">#9E9E9E</Color>