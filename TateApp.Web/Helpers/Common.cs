﻿namespace TateApp.Web.Helpers
{
    using System;
    using System.Configuration;

    public static class Common
    {
        public static string GetStringConnection(string codigo)
        {
            string Conexion = string.Empty;

            //Quito los espacios a la cadena mandante y la transformo a mayúscula
            codigo = codigo.ToUpper().Trim().Replace(" ", string.Empty);

            //Obtengo la conexión
            try
            {
                Conexion = ConfigurationManager.ConnectionStrings[codigo].ConnectionString.ToString().Trim();
            }
            catch (Exception ex)
            {
                Conexion = string.Empty;
            }

            return Conexion;
        }

        public static string GetStringConnectionPrincipal()
        {
            string Conexion = GetStringConnection("BaseApp");
            return Conexion;
        }


        public static int GetVersionMinima()
        {
            return int.Parse(ConfigurationManager.AppSettings["VersionMinima"].ToString().Trim());
        }

        public static int GetVersionActual()
        {
            return int.Parse(ConfigurationManager.AppSettings["VersionActual"].ToString().Trim());
        }

        public static string GetVersionActualNombre()
        {
            return ConfigurationManager.AppSettings["VersionActualNombre"].ToString().Trim();
        }

        public static string GetURL()
        {
            return ConfigurationManager.AppSettings["URL"].ToString().Trim();
        }

        public static string GetURLSoporte()
        {
            return ConfigurationManager.AppSettings["URLSoporte"].ToString().Trim();
        }

        public static string GetURLRegistro() 
        {
            return ConfigurationManager.AppSettings["URLRegistro"].ToString().Trim();
        }

        public static string GetFirebaseKey()
        {
            return ConfigurationManager.AppSettings["FirebaseKey"].ToString().Trim();
        }

        public static string GetFirebaseUri()
        {
            return ConfigurationManager.AppSettings["FirebaseUri"].ToString().Trim();
        }

        public static string GetConfigKey(string clave)
        {
            return ConfigurationManager.AppSettings[clave].ToString().Trim();
        }

        public static bool IsDigitsOnly(string valor) {
            foreach (char item in valor)
            {
                if (item < '0' || item > '9')
                    return false;
            }
            return true;
        }
    }

}