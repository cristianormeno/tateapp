﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using TateApp.Web.Helpers;
using TateApp.Web.Models;
using TateApp.Web.Models.Externas;
using TateApp.Web.Models.Externas.Perfiles;
using TateApp.Web.Models.Externas.Usuario;

namespace TateApp.Web.App_Code.Externas.Usuarios
{
    public static class dalUsuarios
    {
        internal static DefaultModelRespnse ConfigurarUsuario(ConfigurarUsuarioRequest request)
        {
            DefaultModelRespnse response = new DefaultModelRespnse();

            // Obtengo la conexión
            string conexion = TateApp.Web.Helpers.Common.GetStringConnectionPrincipal();

            if (string.IsNullOrEmpty(conexion))
            {
                response.resultado = "error";
                response.mensaje= "No pudo obtenerse la conexión";
                return response;
            }

            DataSet dataSet = new DataSet();
            try
            {
                //Preparo el parámetro json
                string parametros = Newtonsoft.Json.JsonConvert.SerializeObject(request);
                List<SqlParametro> Parametros = new List<SqlParametro>();

                SqlParametro sqlParametro = new SqlParametro()
                {
                    Name = "@parametros",
                    Type = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    Size = -1,
                    Value = parametros
                };

                Parametros.Add(sqlParametro);
                dataSet = Sql.sqlQueryProcedure(conexion, "spMaestroUsuarios", Parametros);

                DataTable dt = new DataTable();
                dt = dataSet.Tables[0];

                DataRow dr = dt.Rows[0];

                response= JsonConvert.DeserializeObject<DefaultModelRespnse>(dr["respuesta"].ToString());


            }
            catch (Exception e) { 
                response.resultado="error";
                response.mensaje = e.Message.Trim();
            }


            return response;
        }

        internal static List<PerfilListadoResponse> ListadoPerfiles()
        {
            List<PerfilListadoResponse> response = new List<PerfilListadoResponse>();

            // Obtengo la conexión
            string conexion = TateApp.Web.Helpers.Common.GetStringConnectionPrincipal();

            DataSet dataSet = new DataSet();
            try
            {
                //Preparo la consulta

                string query = String.Format("select [respuesta] = ISNULL( ( Select app.codigo as [app], p.datos as [perfiles] From ( Select  p.App as codigo From Perfiles p (nolock) Group by p.App ) app Outer Apply( Select p.codigo as codigo, m.datos as menu From Perfiles p (nolock) Outer Apply( Select m.Id as id, RTRIM(t.codigo) as tipoMenu, RTRIM(m.Titulo) as titulo, RTRIM(m.url) as destino, RTRIM(m.descripcion) as descripcion From PerfilMenuMobiles pm (nolock) Inner Join MenuMobile m (nolock) On p.Id=pm.PerfilId And pm.MenuId=m.Id And m.Activo=1 Inner Join MenuMobileTipos t (nolock) On m.TipoId=t.Id Order by m.GrupoId, m.Orden For JSON PATH )m (datos) where app.codigo=p.App For JSON PATH )p (datos) For json path ),'[]')");
                dataSet = Sql.sqlQueryTextCon(query,360,conexion);

                DataTable dt = new DataTable();
                dt = dataSet.Tables[0];

                DataRow dr = dt.Rows[0];

                response = JsonConvert.DeserializeObject<List<PerfilListadoResponse>>(dr["respuesta"].ToString());


            }
            catch (Exception e){}


            return response;
        }
    }
}