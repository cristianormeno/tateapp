﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TateApp.Web.App_Code.Externas.Usuarios;
using TateApp.Web.Models.Externas;
using TateApp.Web.Models.Externas.Perfiles;
using TateApp.Web.Models.Externas.Usuario;

namespace TateApp.Web.Controllers.Externas
{
    [RoutePrefix("tate/externo")]
    public class UsuariosController : ApiController
    {
        [HttpPost]
        [Route("usuarios/configurar")]
        public IHttpActionResult ConfigurarUsuario(ConfigurarUsuarioRequest request)
        {
            if (request == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            DefaultModelRespnse response = new DefaultModelRespnse();

            //Obtengo la respuesta
            response = dalUsuarios.ConfigurarUsuario(request);

            return Ok(response);


        }

        [HttpGet]
        [Route("perfiles/listado")]
        public IHttpActionResult ListadoPerfiles()
        {


            List<PerfilListadoResponse> response = new List<PerfilListadoResponse>();

            //Obtengo la respuesta
            response = dalUsuarios.ListadoPerfiles();

            return Ok(response);


        }
    }
}
