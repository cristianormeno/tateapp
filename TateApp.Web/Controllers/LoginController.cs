﻿namespace TateApp.Web.Controllers
{
    using TateApp.Common.Models;
    using System.Net;
    using System.Web.Http;
    using TateApp.Web.App_Code.Login;

    [RoutePrefix("tate")]

    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public IHttpActionResult LogIn(LoginRequest loginRequest)
        {
            if (loginRequest == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            //Variable de Respuesta
            LoginResponse Respuesta = new LoginResponse();

            //Obtengo la respuesta
            Respuesta = clsLogin.Login(loginRequest);

            return Ok(Respuesta);


        }
    }
}