﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TateApp.Web.Models.Externas.Perfiles
{
    

    // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class PerfilListadoResponsePerfilMenu
    {
        public int id { get; set; }
        public string tipoMenu { get; set; }
        public string titulo { get; set; }
        public string destino { get; set; }
        public string descripcion { get; set; }
    }

    public class PerfilListadoResponsePerfil
    {
        public string codigo { get; set; }
        public List<PerfilListadoResponsePerfilMenu> menu { get; set; }
    }

    public class PerfilListadoResponse
    {
        public string app { get; set; }
        public List<PerfilListadoResponsePerfil> perfiles { get; set; }
    }


}