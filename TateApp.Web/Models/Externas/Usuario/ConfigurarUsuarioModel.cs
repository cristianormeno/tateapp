﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TateApp.Web.Models.Externas.Usuario
{
    public class ConfigurarUsuarioRequest
    {
        public string accion { get; set; }
        public ConfigurarUsuarioRequestUsuario usuario { get; set; }
        public string app { get; set; }
        public List<string> perfiles { get; set; }
    }

    public class ConfigurarUsuarioRequestUsuario
    {
        public string usuario { get; set; }
        public string password { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
    }

}