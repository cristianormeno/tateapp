﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TateApp.Web.Models.DatosUsuario
{
    public class DatosUsuario_Request
    {
        public long Id { get; set; } = 0;
    }

    public class DatosUsuario_Response
    {
        public string codigo { get; set; } = string.Empty;
        public string nombre { get; set; } = string.Empty;
        public string apellido { get; set; } = string.Empty;
    }
}