﻿namespace TateApp.Web.App_Code.Login
{
    using TateApp.Common.Models;
    using TateApp.Web.Helpers;
    using TateApp.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using TateApp.Web.Models.DatosUsuario;

    public static class dalLogin
    {
        public static DataSet Login(string conexion, LoginRequest loginRequest)
        {
            DataSet dataSet = new DataSet();
            try
            {
                //Preparo el parámetro json
                string parametros = Newtonsoft.Json.JsonConvert.SerializeObject(loginRequest);

                List<SqlParametro> Parametros = new List<SqlParametro>();

                SqlParametro sqlParametro = new SqlParametro()
                {
                    Name = "@Parametros",
                    Type = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    Size = -1,
                    Value = parametros
                };

                Parametros.Add(sqlParametro);
                dataSet = Sql.sqlQueryProcedure(conexion, "spLogin", Parametros);
            }
            catch (Exception ex)
            { }



            return dataSet;






        }

        public static DataSet DatosUsuario(DatosUsuario_Request request)
        {
            // Obtengo la conexión
            string conexion = Common.GetStringConnectionPrincipal();

            DataSet dataSet = new DataSet();
            try
            {
                //Preparo el parámetro json
                string parametros = Newtonsoft.Json.JsonConvert.SerializeObject(request);

                List<SqlParametro> Parametros = new List<SqlParametro>();

                SqlParametro sqlParametro = new SqlParametro()
                {
                    Name = "@Parametros",
                    Type = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    Size = -1,
                    Value = parametros
                };

                Parametros.Add(sqlParametro);
                dataSet = Sql.sqlQueryProcedure(conexion, "spDatosUsuario", Parametros);
            }
            catch (Exception ex)
            { }



            return dataSet;






        }
    }


}