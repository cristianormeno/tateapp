﻿namespace TateApp.Web.App_Code.Login
{
    using Microsoft.Ajax.Utilities;
    using Newtonsoft.Json;
    using TateApp.Common.Models;
    using TateApp.Web.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public static class clsLogin
    {
        public static LoginResponse Login(LoginRequest loginRequest)
        {
            // Obtengo la conexión
            string conexion = Common.GetStringConnectionPrincipal();

            //Variable de Respuesta
            LoginResponse loginResponse = new LoginResponse();

            if (string.IsNullOrEmpty(conexion))
            {
                loginResponse.Respuesta.Resultado = "ERROR";
                loginResponse.Respuesta.Mensaje = "No pudo obtenerse la conexión";
                return loginResponse;
            }
            else
            {

                DataSet dataSet = new DataSet();

                dataSet = dalLogin.Login(conexion, loginRequest);

                if (dataSet.Tables.Count > 0)
                {
                    loginResponse = JsonConvert.DeserializeObject<LoginResponse>(dataSet.Tables[0].Rows[0]["Respuesta"].ToString());
                    loginResponse.Sistema = new ResponseSistema()
                    {
                        VersionMinima = Common.GetVersionMinima(),
                        VersionActual = Common.GetVersionActual(),
                        VersionActualNombre = Common.GetVersionActualNombre(),
                        URL = Common.GetURL(),
                        URLRegistro = Common.GetURLRegistro()
                    };
                    if (loginResponse.Respuesta.Resultado.ToString().ToUpper() == "OK")
                    {
                        // Genero el Token WebApi
                        var token = TokenGenerator.GenerateTokenJwt(loginRequest.Usuario.Usuario);
                        loginResponse.Usuario.TokenApiWeb = token;

                        
                        // Si existe menu home, colocarlo también como url por defecto
                        foreach (ResponseMenu item in loginResponse.Menu)
                        {
                            if (item.Titulo.ToLower() == "home" && item.Tipo.ToLower()=="web")
                            {
                                if (!string.IsNullOrEmpty(item.Destino))
                                {
                                    loginResponse.Sistema.URL = item.Destino;
                                    break;
                                }
                            }
                            if (item.Titulo.ToLower() == "inicio" && item.Tipo.ToLower() == "web")
                            {
                                if (!string.IsNullOrEmpty(item.Destino))
                                {
                                    loginResponse.Sistema.URL = item.Destino;
                                    break;
                                }
                            }
                        }

                        loginResponse.Menu = ConfigurarMenu(loginResponse.Menu);
                    }
                }
                else
                {
                    loginResponse = new LoginResponse()
                    {
                        Respuesta = new ResponseResultado()
                        {
                            Resultado = "ERROR",
                            Mensaje = "Algo salió mal, consulte con el administrador del sistema"
                        },
                        Usuario = new ResponseUsuario()
                        {
                            UsuarioId = 0,
                            Nombre = string.Empty
                        },
                        Sistema = new ResponseSistema()
                        {
                            VersionMinima = Common.GetVersionMinima(),
                            VersionActual = Common.GetVersionActual(),
                            VersionActualNombre = Common.GetVersionActualNombre()
                        }

                    };
                }





                return loginResponse;
            }

        }

        private static List<ResponseMenu> ConfigurarMenu(List<ResponseMenu> Origen)
        {
            List<ResponseMenu> menu = new List<ResponseMenu>();
            int i = 0;
            int home = -1;
            int notificaciones = -1;
            int soporte = -1;
            int salir = -1;

            // buscar la existencia de menús de home,notificaciones,soporte y salir
            home = Origen.FindIndex(item => item.Titulo.ToLower().Trim() == "home");
            if (home < 0) 
            {
                home = Origen.FindIndex(item => item.Titulo.ToLower().Trim() == "inicio");
            }


            notificaciones = Origen.FindIndex(item => item.Titulo.ToLower().Trim() == "notificaciones");
            soporte = Origen.FindIndex(item => item.Titulo.ToLower().Trim() == "soporte");
            salir = Origen.FindIndex(item => item.Titulo.ToLower().Trim() == "salir");

            foreach (ResponseMenu item in Origen) {
                if (i == 0) 
                {// en la primera posición debería ir el menú Home 
                    if (home < 0)
                    {
                        menu.Add(AddMenuStandard("home"));
                        home = 1;

                        menu.Add(AddMenuStandard("notificaciones"));
                        notificaciones = 2;

                    }                    
                }
                if (i == 1)
                {
                    if (notificaciones < 0)
                    {
                        // agrego el menú notificaciones
                        menu.Add(AddMenuStandard("notificaciones"));
                        notificaciones = 2;
                    }
                }
                menu.Add(item);
                i++;
            }
            //if (soporte < 0)
            //{
            //    // agregro el menú soporte
            //    menu.Add(AddMenuStandard("soporte"));
            //    soporte = 1;                
            //}

            // agrego el menú salir
            menu.Add(AddMenuStandard("salir"));
            salir = 2;

            return menu;
        }

        private static ResponseMenu AddMenuStandard(string codigo)
        {
            ResponseMenu item = new ResponseMenu();
            switch (codigo) 
            {
                case "home":
                    item = new ResponseMenu()
                    {
                        Grupo = string.Empty,
                        GrupoTitulo = string.Empty,
                        Tipo = "app",
                        Icono = "Home",
                        Titulo = "Inicio",
                        Destino = Common.GetURL()
                    };
                    break;
                case "notificaciones":
                    item = new ResponseMenu()
                    {
                        Grupo = string.Empty,
                        GrupoTitulo = string.Empty,
                        Tipo = "app",
                        Icono = "BellRing",
                        Titulo = "Notificaciones"
                    };
                    break;
                case "soporte":
                    item = new ResponseMenu()
                    {
                        Grupo = "Soporte",
                        GrupoTitulo = string.Empty,
                        Tipo = "web",
                        Icono = "FaceAgent",
                        Titulo = "Soporte",
                        Destino = Common.GetURLSoporte(),
                    };
                    break;
                case "salir":
                    item = new ResponseMenu()
                    {
                        Grupo="Cuenta",
                        GrupoTitulo = string.Empty,
                        Tipo = "app",
                        Icono = "AccountCircleOutline",
                        Titulo = "Salir"
                    };
                    break;

            }
            return item;

        }
    }
}