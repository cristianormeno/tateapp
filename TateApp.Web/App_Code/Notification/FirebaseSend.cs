﻿namespace TateApp.Web.App_Code.Notification
{
    using Newtonsoft.Json;
    using TateApp.Web.Helpers;
    using TateApp.Web.Models;
    using System;
    using System.IO;
    using System.Net;
    using System.Text;

    public class FirebaseSend
    {
        public void SendNotificacion(NotificacionSendRequest notificacionSendRequest, ref string ErrorID, ref string ErrorMensaje)
        {
            string FirebaseKey = Common.GetFirebaseKey();
            string FirebaseUri = Common.GetFirebaseUri();

            WebRequest webRequest = WebRequest.Create(FirebaseUri);
            webRequest.Method = "post";


            //serverKey - Key from Firebase cloud messaging server  
            webRequest.Headers.Add(string.Format("Authorization: key={0}", FirebaseKey.Trim()));
            //Sender Id - From firebase project setting  
            //webRequest.Headers.Add(string.Format("Sender: id={0}", "XXXXX.."));
            webRequest.ContentType = "application/json";
            var payload = new
            {
                to = notificacionSendRequest.Token.Trim(),// "e8EHtMwqsZY:APA91bFUktufXdsDLdXXXXXX..........XXXXXXXXXXXXXX",
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = notificacionSendRequest.Mensaje,// "Test",
                    title = notificacionSendRequest.Titulo,// "Test",
                    badge = 1
                },
                data = new
                {
                    Titulo = notificacionSendRequest.Titulo,
                    Mensaje = notificacionSendRequest.Mensaje,
                    MensajeHtml = notificacionSendRequest.MensajeHtml,
                    Semaforo = notificacionSendRequest.Semaforo,
                    NotificacionId = notificacionSendRequest.NotificacionId
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            webRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = webRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = webRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                FirebaseResponseModel Resultado = JsonConvert.DeserializeObject<FirebaseResponseModel>(sResponseFromServer);
                                if (Resultado.failure > 0)
                                {
                                    ErrorID = "FCM Notification Failed";
                                    foreach (var linea in Resultado.results)
                                    {
                                        if (string.IsNullOrEmpty(ErrorMensaje))
                                        {
                                            ErrorMensaje = linea.error;
                                        }
                                    }

                                }


                            }
                    }
                }
            }
        }
    }
}