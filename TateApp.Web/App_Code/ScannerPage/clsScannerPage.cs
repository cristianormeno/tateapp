﻿using TateApp.Common.Models;
using TateApp.Web;
using System;
using TateApp.Web.Models.DatosUsuario;
using System.Data;
using TateApp.Web.App_Code.Login;
using Newtonsoft.Json;

namespace TateApp.Web.App_Code.ScannerPage
{
    public class clsScannerPage
    {
        public static ScannerPageResponse ScannerPage(ScannerPageRequest scannerPageRequest)
        {
            ScannerPageResponse respuesta = new ScannerPageResponse();
            switch (scannerPageRequest.Sistema.ToLower())
            {
                case "magento":
                    switch (scannerPageRequest.Destino.ToLower()) 
                    {
                        case "productomagento":
                            respuesta.URL = "http://tiendas.solucioneserp.net/lamaral/poliflex-membranas.html";
                            break;
                    }
                    
                    break;
                case "tickets":
                    switch (scannerPageRequest.Destino.ToLower())
                    {
                        case "pruebacodigo":
                            respuesta.URL = "http://tickets.solucioneserp.net/pruebacodigo.html?id=xxxcodxxx".Replace("xxxcodxxx", scannerPageRequest.Valor.Trim());
                            break;
                    }
                    break;
                case "tate":
                    //Sistema TATE
                    
                    string clave = string.Empty;
                    string url = string.Empty;

                    //Según el destino busco la clave y la url por defecto
                    switch (scannerPageRequest.Destino.ToLower())
                    {
                        case "consultaproductos":
                            clave = Helpers.Common.GetConfigKey("consultaProductos");
                            url= Helpers.Common.GetConfigKey("consultaProductosURL");
                            break;
                        case "consultaproductospadres":
                            clave = Helpers.Common.GetConfigKey("consultaProductosPadres");
                            url = Helpers.Common.GetConfigKey("consultaProductosPadresURL");
                            break;
                        case "consultaclientes":
                            clave = Helpers.Common.GetConfigKey("consultaClientes");
                            url = Helpers.Common.GetConfigKey("consultaClientesURL");
                            break;
                        case "entregatransportista":
                            clave = Helpers.Common.GetConfigKey("entregatransportista");
                            url = Helpers.Common.GetConfigKey("entregatransportistaURL");
                            break;
                    }

                    
                    if (!string.IsNullOrEmpty(clave))
                    {
                        //el destino está entre los configurados
                        
                        //verifico si el valor escaneado es una URL que contiene la página configurada
                        if (scannerPageRequest.Valor.Trim().ToLower().Contains(clave))
                        {
                            // Si es una URL válida, devuelvo la url escaneada
                            respuesta.URL = scannerPageRequest.Valor.Trim();
                        }
                        else
                        {
                            //no es una url configurada

                            //verifico si es un código lo que ha escaneado (solo debe contener números)
                            if (Helpers.Common.IsDigitsOnly(scannerPageRequest.Valor.Trim()))
                            {
                                // es un código
                                // reemplazo el código en la url por defecto
                                respuesta.URL = url.Replace("xxxcodxxx", scannerPageRequest.Valor.Trim());
                            }
                            else
                            {
                                switch (scannerPageRequest.Destino.ToLower())
                                {
                                    case "entregatransportista":
                                        DatosUsuario_Request usuRequest = new DatosUsuario_Request()
                                        {
                                            Id = scannerPageRequest.UsuarioId
                                        };
                                        DatosUsuario_Response usuResponse = new DatosUsuario_Response();

                                        DataSet dataSet = dalLogin.DatosUsuario(usuRequest);
                                        if (dataSet.Tables.Count > 0)
                                        {
                                            try { usuResponse = JsonConvert.DeserializeObject<DatosUsuario_Response>(dataSet.Tables[0].Rows[0][0].ToString()); } catch (Exception ex) { }
                                            
                                        }

                                        respuesta.URL = url.Replace("xxxcodxxx", scannerPageRequest.Valor.Trim()).Replace("#", "-").Replace("xxxusuxxx",usuResponse.codigo.Trim());


                                        break;
                                    default:
                                        //devuelvo una url vacía
                                        respuesta.URL = "";
                                        break;

                                }
                                
                            }                            
                        }
                    }
                    else
                    {
                        //devuelvo una url vacía
                        respuesta.URL = "";
                    }

                    
                    break;
                    
            }


            return respuesta;
        }
    }
}